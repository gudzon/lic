﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace WebApplication11.Models
{
    [Table("DomainSet")]
    public partial class DomainSet
    {
        [Key]
        [Display(Name = "Домен")]
        public int DomainId { get; set; }

        [Required (ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(15, MinimumLength = 3, ErrorMessage = "от 3 до 15 символов")]
        [Display(Name = "Домен")]
        [Remote("IsDomainName", "DomainSets", ErrorMessage = "Такой домен уже есть")]
        public string DomainName { get; set; }

        public virtual ICollection<ComputerSet> ComputerSet { get; set; }
    }
}