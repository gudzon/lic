﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace WebApplication11.Models
{
    [Table("ChangelogSet")]
    public class ChangelogSet
    {
        [Key] 
        public int ChangelogId { get; set; }
         
        [Display(Name = "Дата")] 
        public DateTime Date { get; set; }

        [Display(Name = "Комментарий")]
        //Разрешает передавать в поле HTML код
        [AllowHtml]
        public string comment { get; set; }
         
 
    }
}