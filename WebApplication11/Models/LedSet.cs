﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication11.Models
{
    [Table("LedSet")]
    public partial class LedSet
    {
        [Key]
        public int LedId { get; set; }

        [Required(ErrorMessage = "Поле должно быть установлено")]
        public int SalonId { get; set; }

        [Display(Name = "Тип подключения")]
        public string ConnectionType { get; set; }

        [Display(Name = "Модель MOXA")]
        public string ModelMoxa { get; set; }

        [Display(Name = "IP Адрес")]
        public string IpAdress { get; set; }

        [Display(Name = "Программа управления")]
        public string NameSoft { get; set; }

        [Display(Name = "Подключение питания")]
        public string PowerOn { get; set; }

        [Display(Name = "Модель строки")]
        public string LedModel { get; set; }

        [Display(Name = "Размер строки (д/в)")]
        public string LedSize { get; set; }

        [Display(Name = "Поставщик")]
        public string Рrovider { get; set; }

        [Display(Name = "Контактное лицо")]
        public string РroviderContact { get; set; }

        [Display(Name = "Контактный телефон поставщика")]
        public string РroviderCall { get; set; }

        [AllowHtml]
        [Display(Name = "Комментарий")]
        public string Comments { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}",
               ApplyFormatInEditMode = true)]
        [Display(Name = "Дата установки")]
        public DateTime DateInstall { get; set; }
        public virtual SalonSet SalonSet { get; set; }
    }
}