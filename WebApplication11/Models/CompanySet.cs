namespace WebApplication11.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;

    [Table("CompanySet")]
    public partial class CompanySet
    {
        public CompanySet()
        {
            BuyLicSet = new HashSet<BuyLicSet>();
            SalonSet = new HashSet<SalonSet>();
        }

        [Key]
        [Display(Name = "��. ����")]
        public int CompanyId { get; set; }

        [Required(ErrorMessage = "���� ������ ���� ���������")]
        //.[RegularExpression(@"^[�-�]+[�-��-�''-'\s]*$")]
        [StringLength(40, MinimumLength = 5, ErrorMessage = "�� 5 �� 40 ��������")]
        [Remote("IsName", "CompanySets", ErrorMessage = "����� �������� ��� ����")]
        [Display(Name = "��. ����")]
        public string Name { get; set; }


        [Display(Name = "����������� �����")]
        public string address { get; set; }
        [Display(Name = "���")]
        public string inn { get; set; }
        [Display(Name = "��-�� � ���������� �� ���� � ��������� ������ �����")]
        public string numnalog { get; set; }
        [Display(Name = "���")]
        public string kpp { get; set; }
        [Display(Name = "�/�")]
        public string rs { get; set; }
        [Display(Name = "����")]
        public string bank { get; set; }
        [Display(Name = "�/�")]
        public string ks { get; set; }
        [Display(Name = "���")]
        public string bic { get; set; }
        [Display(Name = "����")]
        public string ogrn { get; set; }
        [Display(Name = "��-�� � ���-��� ����������� � �������� ��")]
        public string numgos { get; set; }
        [Display(Name = "��� �����")]
        public string okved { get; set; }
        [Display(Name = "�����")]
        public string oktmo { get; set; }
        [Display(Name = "�����")]
        public string okato { get; set; }
        [Display(Name = "���/����")]
        public string tel { get; set; }
        [Display(Name = "�������� �����")]
        public string mailaddress { get; set; }
        [Display(Name = "����������� ��������")]
        public string gendir { get; set; }

        [Display(Name = "�������� ����")]
        public string files { get; set; }

        public virtual ICollection<BuyLicSet> BuyLicSet { get; set; }

        public virtual ICollection<SalonSet> SalonSet { get; set; }
    }
}
