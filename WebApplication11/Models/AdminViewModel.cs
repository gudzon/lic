﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebApplication11.Models
{
    public class RoleViewModel
    {
        public string Id { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(30, MinimumLength = 5, ErrorMessage = "от 5 до 30 символов")]
        [Display(Name = "Роль")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "от 3 до 60 символов")]
        [Display(Name = "Описание")]
        public string Description { get; set; }
    }

    public class EditUserViewModel
    {
        public string Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "от 6 до 30 символов")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "от 2 до 30 символов")]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "от 2 до 30 символов")]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        public IEnumerable<SelectListItem> RolesList { get; set; }
    }
}