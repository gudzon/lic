namespace WebApplication11.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;

    [Table("SoftCompanySet")]
    public partial class SoftCompanySet
    {
       
        public SoftCompanySet()
        {
            BuyLicSet = new HashSet<BuyLicSet>();
            SoftSet = new HashSet<SoftSet>();
        }
      
        [Key]

        [Display(Name = "������������� ���������")]
        public int SoftCompanyId { get; set; }

        [Required (ErrorMessage = "���� ������ ���� ���������")]
        [StringLength(60, MinimumLength = 5, ErrorMessage = "�� 5 ��������")]
        [Remote("IsName", "SoftCompanySets", ErrorMessage = "����� ������������� �������� ��� ����")]
        [Display(Name = "������������� ���������")]
        public string Name { get; set; }

        public virtual ICollection<BuyLicSet> BuyLicSet { get; set; }

        public virtual ICollection<SoftSet> SoftSet { get; set; }
    }
}
