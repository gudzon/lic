namespace WebApplication11.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.ComponentModel.DataAnnotations;

    public partial class Model11 : DbContext
    {
        public Model11()
            : base("name=Model11")
        {
        }

        public virtual DbSet<BuyLicSet> BuyLicSet { get; set; }
        public virtual DbSet<CompanySet> CompanySet { get; set; }
        public virtual DbSet<ComputerSet> ComputerSet { get; set; }
        public virtual DbSet<DomainSet> DomainSet { get; set; }
        public virtual DbSet<ProviderSet> ProviderSet { get; set; }
        public virtual DbSet<SalonSet> SalonSet { get; set; }
        public virtual DbSet<SoftCompanySet> SoftCompanySet { get; set; }
        public virtual DbSet<SoftSet> SoftSet { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompanySet>()
                .HasMany(e => e.BuyLicSet)
                .WithRequired(e => e.CompanySet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanySet>()
                .HasMany(e => e.SalonSet)
                .WithRequired(e => e.CompanySet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ComputerSet>()
                .HasMany(e => e.BuyLicSet)
                .WithRequired(e => e.ComputerSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProviderSet>()
                .HasMany(e => e.BuyLicSet)
                .WithRequired(e => e.ProviderSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SalonSet>()
                .HasMany(e => e.BuyLicSet)
                .WithRequired(e => e.SalonSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SalonSet>()
                .HasMany(e => e.ComputerSet)
                .WithRequired(e => e.SalonSet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SoftCompanySet>()
                .HasMany(e => e.BuyLicSet)
                .WithRequired(e => e.SoftCompanySet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SoftCompanySet>()
                .HasMany(e => e.SoftSet)
                .WithRequired(e => e.SoftCompanySet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SoftSet>()
                .HasMany(e => e.BuyLicSet)
                .WithRequired(e => e.SoftSet)
                .WillCascadeOnDelete(false);
        }

        public System.Data.Entity.DbSet<WebApplication11.Models.LicBuy> LicBuys { get; set; }

        public System.Data.Entity.DbSet<WebApplication11.Models.FirmSet> FirmSets { get; set; }

        public System.Data.Entity.DbSet<WebApplication11.Models.ChangelogSet> ChangelogSets { get; set; }

        public System.Data.Entity.DbSet<WebApplication11.Models.GateSet> GateSets { get; set; }

        public System.Data.Entity.DbSet<WebApplication11.Models.LedSet> LedSets { get; set; }

        public System.Data.Entity.DbSet<WebApplication11.Models.Apk> Apks { get; set; }
    }
}
