﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication11.Models
{
    [Table("LicBuy")]
    public class LicBuy
    {
        [Key]
        public int LicBuyId { get; set; }

        [Display(Name = "Наименование")]
        public int SoftId { get; set; }

        [Display(Name = "Колличество")]
        [Range(1, 100, ErrorMessage = "От 1 до 100")]
        public int Quantity { get; set; }

        [Display(Name = "Стоимость"), DataType(DataType.Currency)]
        public int Price { get; set; }

        [Display(Name = "Производитель программы")]
        public int SoftCompanyId { get; set; }

        [Display(Name = "Юр. лицо")]
        public int CompanyId { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}",
               ApplyFormatInEditMode = true)]
        [Display(Name = "Дата поставки")]
        public DateTime DateBuy { get; set; }

        [Display(Name = "Поставщик")]
        public int ProviderId { get; set; }

        public virtual CompanySet CompanySet { get; set; } 

        public virtual ProviderSet ProviderSet { get; set; } 

        public virtual SoftSet SoftSet { get; set; }

        public virtual SoftCompanySet SoftCompanySet { get; set; }
    }
}
