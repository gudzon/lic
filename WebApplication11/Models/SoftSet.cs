namespace WebApplication11.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web.Mvc;

    [Table("SoftSet")]
    public partial class SoftSet
    {
        public SoftSet()
        {
            BuyLicSet = new HashSet<BuyLicSet>();
        }

        [Key]
        [Display(Name = "�������� ���������")]
        public int SoftId { get; set; }

        [Required (ErrorMessage = "���� ������ ���� ���������")]
        [StringLength(60, MinimumLength = 2, ErrorMessage = "�� 2 �� 60 ��������")]
        [Display(Name = "������������")]
        [Remote("IsName", "SoftSets", ErrorMessage = "����� ��������� ��� ����")]
        public string Name { get; set; }


        [StringLength(50, MinimumLength = 2, ErrorMessage = "�� 2 �� 50 ��������")]
        [Display(Name = "ID ���������")]
        public string SoftNameId { get; set; }

        [Required(ErrorMessage = "���� ������ ���� ���������")]
        [Display(Name = "������������� ���������")]
        public int SoftCompanyId { get; set; }

        public virtual ICollection<BuyLicSet> BuyLicSet { get; set; }

        public virtual SoftCompanySet SoftCompanySet { get; set; }
    }
}