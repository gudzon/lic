﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication11.Models
{
    [Table("GateSet")]
    public partial class GateSet
    {
        [Key]
        public int GateId { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [Display(Name = "Внешний IP адрес")]
        public string IpAdress { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [Display(Name = "Внутренний IP адрес")]
        public string IpAdressLocal { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [Display(Name = "Модель оборудования")]
        public string Model { get; set; }

        [Required(ErrorMessage = "Поле должно быть установлено")]
        public int SalonId { get; set; }

        public virtual SalonSet SalonSet { get; set; }
    }
}