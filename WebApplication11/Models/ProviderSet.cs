namespace WebApplication11.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;

    [Table("ProviderSet")]
    public partial class ProviderSet
    {
        public ProviderSet()
        {
            BuyLicSet = new HashSet<BuyLicSet>();
        }

        [Key]
        [Display(Name = "���������")]
        public int ProviderId { get; set; }

        [Required(ErrorMessage = "���� ������ ���� ���������")]
        [StringLength(60, MinimumLength = 5, ErrorMessage = "�� 5 �� 60 ��������")]
        [Remote("IsName", "ProviderSets", ErrorMessage = "����� ��������� ��� ����")]
        [Display(Name = "���������")]
        public string Name { get; set; }

        [Display(Name = "����� �����")]
        public string site { get; set; }

        [Display(Name = "��� ���������")]
        public string manager { get; set; }

        [Display(Name = "�������")]
        public string tel { get; set; }

        [Display(Name = "�������� �����")]
        public string mail { get; set; }

        [Display(Name = "ICQ")]
        public string icq { get; set; }

        [Display(Name = "������ �������")]
        public string lk { get; set; }

        [Display(Name = "��� ������������")]
        public string login { get; set; }

        [Display(Name = "������")]
        public string pass { get; set; }

        [Display(Name = "�����������")]
        public string comment { get; set; }

        public virtual ICollection<BuyLicSet> BuyLicSet { get; set; }
    }
}
