﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication11.Models
{
    public class FirmSet
    {
        [Key]
        [Display(Name = "Фирма")]
        public int FirmId { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(15, MinimumLength = 3, ErrorMessage = "от 3 до 15 символов")]
        [Display(Name = "Фирма")]
        [Remote("IsFirmName", "FirmSets", ErrorMessage = "Такая фирма уже есть")]
        public string FirmName { get; set; }

    }
}