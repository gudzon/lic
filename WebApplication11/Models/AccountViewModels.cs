﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication11.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required (ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "от 6 до 30 символов")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required (ErrorMessage = "Поле должно быть заполнено")]
        public string Provider { get; set; }

        [Required (ErrorMessage = "Поле должно быть заполнено")]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }
    }

    public class ForgotViewModel
    {
        [Required (ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "от 6 до 30 символов")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required (ErrorMessage = "Введите почтовый адрес")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "от 6 до 30 символов")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Введите пароль")]
        [DataType(DataType.Password)]
        [StringLength(60, MinimumLength = 8, ErrorMessage = "от 8 символов (буквы в разных регистрах + цифры)")]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required (ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "от 6 до 30 символов")]
        [System.Web.Mvc.Remote("IsEmail", "RegisterViewModel", ErrorMessage = "Такое имя пользователя уже есть")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required (ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(100, ErrorMessage = "{0} должен быть более {2} символов", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Повторить пароль")]
        [Compare("Password", ErrorMessage = "Новый пароль и подтверждение пароля не совпадают.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "от 2 до 30 символов")]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }
        
        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "от 2 до 30 символов")]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required (ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "от 6 до 30 символов")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required (ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(100, ErrorMessage = "{0} должен быть более {2} символов", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [DataType(DataType.Password)]
        [Display(Name = "Повторить пароль")]
        [Compare("Password", ErrorMessage = "Новый пароль и подтверждение пароля не совпадают.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required (ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "от 6 до 30 символов")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}