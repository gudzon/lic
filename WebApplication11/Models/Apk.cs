﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication11.Models
{
    [Table("Apk")]
    public class Apk
    {
        [Key]
        public int IdApk { get; set; }
         
        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [Display(Name = "Название программы")]
        public string NameApk { get; set; }
         
        [Display(Name = "Описание")]
        public string DescriptionApk { get; set; }
         
        [Display(Name = "Версия")]
        public string VersionApk { get; set; }
         
        [Display(Name = "Файл")]
        public string FileApk { get; set; }
         
    }
}