namespace WebApplication11.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;

    [Table("SalonSet")]
    public partial class SalonSet
    {
        public SalonSet()
        {
            BuyLicSet = new HashSet<BuyLicSet>();
            ComputerSet = new HashSet<ComputerSet>();
        }

        [Key]
        [Display(Name = "�������������")]
        public int SalonId { get; set; }

        [Required (ErrorMessage = "���� ������ ���� ���������")]
        [StringLength(60, MinimumLength = 5, ErrorMessage = "�� 5 ��������")]
        [Display(Name = "�������������")]
        [Remote("IsSalonName", "SalonSets", ErrorMessage = "����� ������������� ��� ����")]
        public string Name { get; set; }

        [Required (ErrorMessage = "���� ������ ���� ���������")]
        [StringLength(100, MinimumLength = 10, ErrorMessage = "�� 10 ��������")]
        [Display(Name = "�����")]
        public string Address { get; set; }

        [Required (ErrorMessage = "���� ������ ���� ���������")]
        [Display(Name = "�������"), DataType(DataType.PhoneNumber)]
        [StringLength(50, MinimumLength = 11, ErrorMessage = "� ������� +7 (���) ���-��-��, �� ����� 50 ��������")]
        [Phone]
        public string Phone { get; set; }

        [Required(ErrorMessage = "���� ������ ���� ���������")]
        [Display(Name = "��. ����")]
        public int CompanyId { get; set; }

        public virtual ICollection<BuyLicSet> BuyLicSet { get; set; }

        public virtual CompanySet CompanySet { get; set; }

        public virtual ICollection<ComputerSet> ComputerSet { get; set; }

        public virtual ICollection<GateSet> GateSet { get; set; }
    }
}
