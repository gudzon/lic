namespace WebApplication11.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BuyLicSet")]
    public partial class BuyLicSet
    {
        [Key]
        public int BuyLicId { get; set; }

        public int SoftId { get; set; }

        [Display(Name = "�����������")]
        [Range(1, 100, ErrorMessage = "��������"), DataType(DataType.Currency)]
        public int Quantity { get; set; }

        [Display(Name = "����")]
        public int Price { get; set; }

        public int SoftCompanyId { get; set; }

        public int CompanyId { get; set; }

        [Column(TypeName = "date"), DataType(DataType.Date)]
        [Display(Name = "���� �������")]
        public DateTime DateBuy { get; set; }

        public int SalonId { get; set; }

        public int ComputerId { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "���� ���������"), DataType(DataType.Date)]
        public DateTime DateActivation { get; set; }

        [Required (ErrorMessage = "���� ������ ���� ���������")]
        [Display(Name = "����� ��������")]
        public string Invoice { get; set; }

        [Required (ErrorMessage = "���� ������ ���� ���������")]
        [Display(Name = "��������� ��������"), DataType(DataType.Upload)]
        public string Docs { get; set; }

        public int ProviderId { get; set; }

        public virtual CompanySet CompanySet { get; set; }

        public virtual ComputerSet ComputerSet { get; set; }

        public virtual ProviderSet ProviderSet { get; set; }

        public virtual SalonSet SalonSet { get; set; }

        public virtual SoftSet SoftSet { get; set; }

        public virtual SoftCompanySet SoftCompanySet { get; set; }
    }
}
