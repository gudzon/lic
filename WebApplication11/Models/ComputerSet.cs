using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace WebApplication11.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table ("ComputerSet")]
    public partial class ComputerSet
    {
        public ComputerSet()
        {
            BuyLicSet = new HashSet<BuyLicSet>();
        }

        [Key]
        [Display(Name = "��� ����������")]
        public int ComputerId { get; set; }

        [Required (ErrorMessage = "���� ������ ���� ���������")]
        [StringLength(15, MinimumLength = 2, ErrorMessage = "�� 2 �� 15 ��������")]
        [Display(Name = "��� ����������")]
        [Remote("IsComputerName", "ComputerSets", ErrorMessage = "����� ��� ���������� ��� ����")]
        public string Name { get; set; }

        [Required(ErrorMessage = "���� ������ ���� �����������")]
        public int SalonId { get; set; }

        [Required(ErrorMessage = "���� ������ ���� �����������")]
        [Display(Name = "�����")]
        public int DomainId { get; set; }

        public virtual ICollection<BuyLicSet> BuyLicSet { get; set; }

        public virtual SalonSet SalonSet { get; set; }
        public virtual DomainSet DomainSet { get; set; }
    }
}