﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class ProviderSetsController : Controller
    {
        private Model11 db = new Model11();

        // GET: ProviderSets
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Index()
        {
            return View(db.ProviderSet.ToList());
        }
        public JsonResult IsName(string Name)
        {
            return Json(!db.ProviderSet.Any(x => x.Name == Name),
                                                 JsonRequestBehavior.AllowGet);
        }


        //// Загружаем документы на сервер
        //[HttpPost]
        //public ActionResult Upload(HttpPostedFileBase uploadfile)
        //{
        //    if (uploadfile != null && uploadfile.ContentLength > 0)
        //    {
        //        var fileName = Path.GetFileName(uploadfile.FileName);
        //        uploadfile.SaveAs(Path.Combine(Server.MapPath("~/Content/"), fileName));
        //    }
        //    return RedirectToAction("Index");
        //}





        // GET: ProviderSets/Details/5
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProviderSet providerSet = db.ProviderSet.Find(id);
            if (providerSet == null)
            {
                return HttpNotFound();
            }
            return View(providerSet);
        }

        // GET: ProviderSets/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProviderSets/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProviderId,Name,site,manager,tel,mail,icq,lk,login,pass,comment")] ProviderSet providerSet)
        {
            if (ModelState.IsValid)
            {
                db.ProviderSet.Add(providerSet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(providerSet);
        }

        // GET: ProviderSets/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProviderSet providerSet = db.ProviderSet.Find(id);
            if (providerSet == null)
            {
                return HttpNotFound();
            }
            return View(providerSet);
        }

        // POST: ProviderSets/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProviderId,Name,site,manager,tel,mail,icq,lk,login,pass,comment")] ProviderSet providerSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(providerSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(providerSet);
        }

        // GET: ProviderSets/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProviderSet providerSet = db.ProviderSet.Find(id);
            if (providerSet == null)
            {
                return HttpNotFound();
            }
            return View(providerSet);
        }

        // POST: ProviderSets/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProviderSet providerSet = db.ProviderSet.Find(id);
            db.ProviderSet.Remove(providerSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
