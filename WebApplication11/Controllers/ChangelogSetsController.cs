﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class ChangelogSetsController : Controller
    {
        private Model11 db = new Model11();

        // GET: ChangelogSets
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Index()
        {
            // Сортировка от новых к старым
            return View(db.ChangelogSets.OrderByDescending(c => c.ChangelogId).Take(20).ToList());
             
        }

        // GET: ChangelogSets/Details/5
        [Authorize(Roles = "DevAdmin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChangelogSet changelogSet = db.ChangelogSets.Find(id);
            if (changelogSet == null)
            {
                return HttpNotFound();
            }
            return View(changelogSet);
        }

        // GET: ChangelogSets/Create
        [Authorize(Roles = "DevAdmin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: ChangelogSets/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "DevAdmin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ChangelogId,Date,comment")] ChangelogSet changelogSet)
        {
            if (ModelState.IsValid)
            {
                db.ChangelogSets.Add(changelogSet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(changelogSet);
        }

        // GET: ChangelogSets/Edit/5
        [Authorize(Roles = "DevAdmin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChangelogSet changelogSet = db.ChangelogSets.Find(id);
            if (changelogSet == null)
            {
                return HttpNotFound();
            }
            return View(changelogSet);
        }

        // POST: ChangelogSets/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "DevAdmin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ChangelogId,Date,comment")] ChangelogSet changelogSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(changelogSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(changelogSet);
        }

        // GET: ChangelogSets/Delete/5
        [Authorize(Roles = "DevAdmin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChangelogSet changelogSet = db.ChangelogSets.Find(id);
            if (changelogSet == null)
            {
                return HttpNotFound();
            }
            return View(changelogSet);
        }

        // POST: ChangelogSets/Delete/5
        [Authorize(Roles = "DevAdmin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChangelogSet changelogSet = db.ChangelogSets.Find(id);
            db.ChangelogSets.Remove(changelogSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
