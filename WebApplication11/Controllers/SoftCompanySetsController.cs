﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class SoftCompanySetsController : Controller
    {
        private Model11 db = new Model11();

        // GET: SoftCompanySets
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Index()
        {
            return View(db.SoftCompanySet.ToList());
        }

        public JsonResult IsName(string Name)
        {
            return Json(!db.SoftCompanySet.Any(x => x.Name == Name),
                                                 JsonRequestBehavior.AllowGet);
        }

        // GET: SoftCompanySets/Details/5
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SoftCompanySet softCompanySet = db.SoftCompanySet.Find(id);
            if (softCompanySet == null)
            {
                return HttpNotFound();
            }
            return View(softCompanySet);
        }

        // GET: SoftCompanySets/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: SoftCompanySets/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SoftCompanyId,Name")] SoftCompanySet softCompanySet)
        {
            if (ModelState.IsValid)
            {
                db.SoftCompanySet.Add(softCompanySet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(softCompanySet);
        }

        // GET: SoftCompanySets/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SoftCompanySet softCompanySet = db.SoftCompanySet.Find(id);
            if (softCompanySet == null)
            {
                return HttpNotFound();
            }
            return View(softCompanySet);
        }

        // POST: SoftCompanySets/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SoftCompanyId,Name")] SoftCompanySet softCompanySet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(softCompanySet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(softCompanySet);
        }

        // GET: SoftCompanySets/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SoftCompanySet softCompanySet = db.SoftCompanySet.Find(id);
            if (softCompanySet == null)
            {
                return HttpNotFound();
            }
            return View(softCompanySet);
        }

        // POST: SoftCompanySets/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SoftCompanySet softCompanySet = db.SoftCompanySet.Find(id);
            db.SoftCompanySet.Remove(softCompanySet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
