﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class HomeController : Controller
    {
        private Model11 db = new Model11();

        // GET: LicBuys
        [Authorize]
        public ActionResult Index()
        {
            var licBuys = db.LicBuys.Include(l => l.CompanySet).Include(l => l.ProviderSet).Include(l => l.SoftCompanySet).Include(l => l.SoftSet);
            return View(licBuys.ToList());
        }

        // GET: LicBuys/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LicBuy licBuy = db.LicBuys.Find(id);
            if (licBuy == null)
            {
                return HttpNotFound();
            }
            return View(licBuy);
        }

        // GET: LicBuys/Create
        public ActionResult Create()
        {
            ViewBag.CompanyId = new SelectList(db.CompanySet, "CompanyId", "Name");
            ViewBag.ProviderId = new SelectList(db.ProviderSet, "ProviderId", "Name");
            ViewBag.SoftCompanyId = new SelectList(db.SoftCompanySet, "SoftCompanyId", "Name");
            ViewBag.SoftId = new SelectList(db.SoftSet, "SoftId", "Name");
            return View();
        }

        // POST: LicBuys/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LicBuyId,SoftId,Quantity,Price,SoftCompanyId,CompanyId,DateBuy,ProviderId")] LicBuy licBuy)
        {
            if (ModelState.IsValid)
            {
                db.LicBuys.Add(licBuy);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CompanyId = new SelectList(db.CompanySet, "CompanyId", "Name", licBuy.CompanyId);
            ViewBag.ProviderId = new SelectList(db.ProviderSet, "ProviderId", "Name", licBuy.ProviderId);
            ViewBag.SoftCompanyId = new SelectList(db.SoftCompanySet, "SoftCompanyId", "Name", licBuy.SoftCompanyId);
            ViewBag.SoftId = new SelectList(db.SoftSet, "SoftId", "Name", licBuy.SoftId);
            return View(licBuy);
        }

        // GET: LicBuys/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LicBuy licBuy = db.LicBuys.Find(id);
            if (licBuy == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyId = new SelectList(db.CompanySet, "CompanyId", "Name", licBuy.CompanyId);
            ViewBag.ProviderId = new SelectList(db.ProviderSet, "ProviderId", "Name", licBuy.ProviderId);
            ViewBag.SoftCompanyId = new SelectList(db.SoftCompanySet, "SoftCompanyId", "Name", licBuy.SoftCompanyId);
            ViewBag.SoftId = new SelectList(db.SoftSet, "SoftId", "Name", licBuy.SoftId);
            return View(licBuy);
        }

        // POST: LicBuys/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LicBuyId,SoftId,Quantity,Price,SoftCompanyId,CompanyId,DateBuy,ProviderId")] LicBuy licBuy)
        {
            if (ModelState.IsValid)
            {
                db.Entry(licBuy).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyId = new SelectList(db.CompanySet, "CompanyId", "Name", licBuy.CompanyId);
            ViewBag.ProviderId = new SelectList(db.ProviderSet, "ProviderId", "Name", licBuy.ProviderId);
            ViewBag.SoftCompanyId = new SelectList(db.SoftCompanySet, "SoftCompanyId", "Name", licBuy.SoftCompanyId);
            ViewBag.SoftId = new SelectList(db.SoftSet, "SoftId", "Name", licBuy.SoftId);
            return View(licBuy);
        }

        // GET: LicBuys/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LicBuy licBuy = db.LicBuys.Find(id);
            if (licBuy == null)
            {
                return HttpNotFound();
            }
            return View(licBuy);
        }

        // POST: LicBuys/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LicBuy licBuy = db.LicBuys.Find(id);
            db.LicBuys.Remove(licBuy);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
