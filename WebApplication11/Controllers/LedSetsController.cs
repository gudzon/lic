﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class LedSetsController : Controller
    {
        private Model11 db = new Model11();

        // GET: LedSets
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Index()
        {
            var ledSets = db.LedSets.Include(l => l.SalonSet);
            return View(ledSets.ToList());
        }

        // GET: LedSets/Details/5
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LedSet ledSet = db.LedSets.Find(id);
            if (ledSet == null)
            {
                return HttpNotFound();
            }
            return View(ledSet);
        }

        // GET: LedSets/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            var salonQuery = from d in db.SalonSet
                             orderby d.Name
                             select d;
            ViewBag.SalonId = new SelectList(salonQuery, "SalonId", "Name");
            return View();
        }

        // POST: LedSets/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LedId,SalonId,ConnectionType,ModelMoxa,IpAdress,NameSoft,PowerOn,LedModel,LedSize,Рrovider,РroviderContact,РroviderCall,Comments,DateInstall")] LedSet ledSet)
        {
            if (ModelState.IsValid)
            {
                db.LedSets.Add(ledSet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SalonId = new SelectList(db.SalonSet, "SalonId", "Name", ledSet.SalonId);
            return View(ledSet);
        }

        // GET: LedSets/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LedSet ledSet = db.LedSets.Find(id);
            if (ledSet == null)
            {
                return HttpNotFound();
            }
            var salonQuery = from d in db.SalonSet
                             orderby d.Name
                             select d;            
            ViewBag.SalonId = new SelectList(salonQuery, "SalonId", "Name", ledSet.SalonId);
            return View(ledSet);
        }

        // POST: LedSets/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LedId,SalonId,ConnectionType,ModelMoxa,IpAdress,NameSoft,PowerOn,LedModel,LedSize,Рrovider,РroviderContact,РroviderCall,Comments,DateInstall")] LedSet ledSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ledSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SalonId = new SelectList(db.SalonSet, "SalonId", "Name", ledSet.SalonId);
            return View(ledSet);
        }

        // GET: LedSets/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LedSet ledSet = db.LedSets.Find(id);
            if (ledSet == null)
            {
                return HttpNotFound();
            }
            return View(ledSet);
        }

        // POST: LedSets/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LedSet ledSet = db.LedSets.Find(id);
            db.LedSets.Remove(ledSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
