﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class FirmSetsController : Controller
    {
        private Model11 db = new Model11();

        // GET: FirmSets
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Index()
        {
            return View(db.FirmSets.ToList());
        }

        public JsonResult IsFirmName(string FirmName, int? FirmId = null)
        {
            return Json(!db.FirmSets.Any(x => x.FirmName == FirmName && x.FirmId != FirmId), JsonRequestBehavior.AllowGet);
        }

        // GET: FirmSets/Details/5
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FirmSet firmSet = db.FirmSets.Find(id);
            if (firmSet == null)
            {
                return HttpNotFound();
            }
            return View(firmSet);
        }

        // GET: FirmSets/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: FirmSets/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FirmId,FirmName")] FirmSet firmSet)
        {
            if (ModelState.IsValid)
            {
                db.FirmSets.Add(firmSet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(firmSet);
        }

        // GET: FirmSets/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FirmSet firmSet = db.FirmSets.Find(id);
            if (firmSet == null)
            {
                return HttpNotFound();
            }
            return View(firmSet);
        }

        // POST: FirmSets/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FirmId,FirmName")] FirmSet firmSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(firmSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(firmSet);
        }

        // GET: FirmSets/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FirmSet firmSet = db.FirmSets.Find(id);
            if (firmSet == null)
            {
                return HttpNotFound();
            }
            return View(firmSet);
        }

        // POST: FirmSets/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FirmSet firmSet = db.FirmSets.Find(id);
            db.FirmSets.Remove(firmSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
