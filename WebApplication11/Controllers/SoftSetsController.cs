﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class SoftSetsController : Controller
    {
        private Model11 db = new Model11();

        // GET: SoftSets
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Index()
        {
            var softSet = db.SoftSet.Include(s => s.SoftCompanySet);
            return View(softSet.ToList());
        }

        [Authorize(Roles = "Admin, ReadUser")]
        public JsonResult IsName(string Name)
        {
            return Json(!db.SoftSet.Any(x => x.Name == Name),
                                                 JsonRequestBehavior.AllowGet);
        }
        // GET: SoftSets/Details/5
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SoftSet softSet = db.SoftSet.Find(id);
            if (softSet == null)
            {
                return HttpNotFound();
            }
            return View(softSet);
        }

        // GET: SoftSets/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            var SoftCompanyQuery = from d in db.SoftCompanySet
                                   orderby d.Name
                                   select d;
            ViewBag.SoftCompanyId = new SelectList(SoftCompanyQuery, "SoftCompanyId", "Name", "SoftNameId");
            return View();
        }

        // POST: SoftSets/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SoftId,Name,SoftCompanyId,SoftNameId")] SoftSet softSet)
        {
            if (ModelState.IsValid)
            {
                db.SoftSet.Add(softSet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SoftCompanyId = new SelectList(db.SoftCompanySet, "SoftCompanyId", "Name", "SoftNameId", softSet.SoftCompanyId);
            return View(softSet);
        }

        // GET: SoftSets/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SoftSet softSet = db.SoftSet.Find(id);
            if (softSet == null)
            {
                return HttpNotFound();
            }
            ViewBag.SoftCompanyId = new SelectList(db.SoftCompanySet, "SoftCompanyId", "Name",   softSet.SoftCompanyId);
            return View(softSet);
        }

        // POST: SoftSets/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SoftId,Name,SoftCompanyId,SoftNameId")] SoftSet softSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(softSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SoftCompanyId = new SelectList(db.SoftCompanySet, "SoftCompanyId", "Name", "SoftNameId", softSet.SoftCompanyId);
            return View(softSet);
        }

        // GET: SoftSets/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SoftSet softSet = db.SoftSet.Find(id);
            if (softSet == null)
            {
                return HttpNotFound();
            }
            return View(softSet);
        }

        // POST: SoftSets/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SoftSet softSet = db.SoftSet.Find(id);
            db.SoftSet.Remove(softSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
