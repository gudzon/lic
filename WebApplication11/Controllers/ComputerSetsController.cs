﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class ComputerSetsController : Controller
    {
        private Model11 db = new Model11();

        // GET: ComputerSets
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Index()
        {
            //ViewBag.Equipment = "class='active open'";
            var computerSet = db.ComputerSet.Include(c => c.SalonSet).OrderByDescending(c => c.SalonId).OrderByDescending(b => b.DomainId);
            return View(computerSet.ToList());
        }

        public JsonResult IsComputerName(string Name)
        {
            return Json(!db.ComputerSet.Any(x => x.Name == Name),
                                                 JsonRequestBehavior.AllowGet);
        }

        // GET: ComputerSets/Details/5
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ComputerSet computerSet = db.ComputerSet.Find(id);
            if (computerSet == null)
            {
                return HttpNotFound();
            }
            return View(computerSet);
        }

        // GET: ComputerSets/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            var salonQuery = from d in db.SalonSet
                             orderby d.Name
                             select d;
            ViewBag.SalonId = new SelectList(salonQuery, "SalonId", "Name");

            var domainQuery = from d in db.DomainSet
                              orderby d.DomainName
                              select d;
            ViewBag.DomainId = new SelectList(domainQuery, "DomainId", "DomainName");

            return View();
        }

        // POST: ComputerSets/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ComputerId,Name,SalonId,DomainId")] ComputerSet computerSet)
        {
            if (ModelState.IsValid)
            {
                db.ComputerSet.Add(computerSet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SalonId = new SelectList(db.SalonSet, "SalonId", "Name", computerSet.SalonId);
            ViewBag.DomainId = new SelectList(db.DomainSet, "DomainId", "DomainName", computerSet.DomainId);
            return View(computerSet);
        }

        // GET: ComputerSets/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ComputerSet computerSet = db.ComputerSet.Find(id);
            if (computerSet == null)
            {
                return HttpNotFound();
            }
            var salonQuery = from d in db.SalonSet
                             orderby d.Name
                             select d;
            ViewBag.SalonId = new SelectList(salonQuery, "SalonId", "Name", computerSet.SalonId);

            //DomainSet domainSet = db.DomainSet.Find(id);
            //if (domainSet == null)
            //{
            //    return HttpNotFound();
            //}
            var domainQuery = from d in db.DomainSet
                              orderby d.DomainName
                              select d; 
            ViewBag.DomainId = new SelectList(domainQuery, "DomainId", "DomainName", computerSet.DomainId);
            return View(computerSet);

        }

        // POST: ComputerSets/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ComputerId,Name,SalonId,DomainId")] ComputerSet computerSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(computerSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SalonId = new SelectList(db.SalonSet, "SalonId", "Name", computerSet.SalonId);
            return View(computerSet);
        }



        // GET: ComputerSets/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ComputerSet computerSet = db.ComputerSet.Find(id);
            if (computerSet == null)
            {
                return HttpNotFound();
            }
            return View(computerSet);
        }

        // POST: ComputerSets/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ComputerSet computerSet = db.ComputerSet.Find(id);
            db.ComputerSet.Remove(computerSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
