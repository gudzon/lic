﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class DomainSetsController : Controller
    {
        private Model11 db = new Model11();

        // GET: DomainSets
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Index()
        {
            return View(db.DomainSet.ToList());
        }

        public JsonResult IsDomainName(string DomainName)
        {
            return Json(!db.DomainSet.Any(x => x.DomainName == DomainName),
                JsonRequestBehavior.AllowGet);
        }
  

        // GET: DomainSets/Details/5
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DomainSet domainSet = db.DomainSet.Find(id);
            if (domainSet == null)
            {
                return HttpNotFound();
            }
            return View(domainSet);
        }

        // GET: DomainSets/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: DomainSets/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DomainId,DomainName")] DomainSet domainSet)
        {
            if (ModelState.IsValid)
            {
                db.DomainSet.Add(domainSet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(domainSet);
        }

        // GET: DomainSets/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DomainSet domainSet = db.DomainSet.Find(id);
            if (domainSet == null)
            {
                return HttpNotFound();
            }
            return View(domainSet);
        }

        // POST: DomainSets/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DomainId,DomainName")] DomainSet domainSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(domainSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(domainSet);
        }

        // GET: DomainSets/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DomainSet domainSet = db.DomainSet.Find(id);
            if (domainSet == null)
            {
                return HttpNotFound();
            }
            return View(domainSet);
        }

        // POST: DomainSets/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DomainSet domainSet = db.DomainSet.Find(id);
            db.DomainSet.Remove(domainSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
