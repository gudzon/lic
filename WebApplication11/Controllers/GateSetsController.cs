﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class GateSetsController : Controller
    {
        private Model11 db = new Model11();

        // GET: GateSets
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Index()
        {
            var gateSets = db.GateSets.Include(g => g.SalonSet);
            return View(gateSets.ToList());
        }

        // GET: GateSets/Details/5
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GateSet gateSet = db.GateSets.Find(id);
            if (gateSet == null)
            {
                return HttpNotFound();
            }
            return View(gateSet);
        }

        // GET: GateSets/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            var salonQuery = from d in db.SalonSet
                             orderby d.Name
                             select d;
            ViewBag.SalonId = new SelectList(salonQuery, "SalonId", "Name");
            return View();
        }

        // POST: GateSets/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "GateId,IpAdress,SalonId,Model,IpAdressLocal")] GateSet gateSet)
        {
            if (ModelState.IsValid)
            {
                db.GateSets.Add(gateSet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SalonId = new SelectList(db.SalonSet, "SalonId", "Name", gateSet.SalonId);
            return View(gateSet);
        }

        // GET: GateSets/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GateSet gateSet = db.GateSets.Find(id);
            if (gateSet == null)
            {
                return HttpNotFound();
            }
            var salonQuery = from d in db.SalonSet
                             orderby d.Name
                             select d; 
            ViewBag.SalonId = new SelectList(salonQuery, "SalonId", "Name", gateSet.SalonId);
            return View(gateSet);
        }

        // POST: GateSets/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "GateId,IpAdress,SalonId,Model,IpAdressLocal")] GateSet gateSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gateSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SalonId = new SelectList(db.SalonSet, "SalonId", "Name", gateSet.SalonId);
            return View(gateSet);
        }

        // GET: GateSets/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GateSet gateSet = db.GateSets.Find(id);
            if (gateSet == null)
            {
                return HttpNotFound();
            }
            return View(gateSet);
        }

        // POST: GateSets/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GateSet gateSet = db.GateSets.Find(id);
            db.GateSets.Remove(gateSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
