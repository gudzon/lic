﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class SalonSetsController : Controller
    {
        private Model11 db = new Model11();

        // GET: SalonSets
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Index()
        {
            var salonSet = db.SalonSet.Include(s => s.CompanySet).OrderByDescending(s=>s.CompanyId);
            return View(salonSet.ToList());
        }

        public JsonResult IsSalonName(string Name)
        {
            return Json(!db.SalonSet.Any(x => x.Name == Name),
                                                 JsonRequestBehavior.AllowGet);
        }

        // GET: SalonSets/Details/5
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalonSet salonSet = db.SalonSet.Find(id);
            if (salonSet == null)
            {
                return HttpNotFound();
            }
            return View(salonSet);
        }

        // GET: SalonSets/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.CompanyId = new SelectList(db.CompanySet, "CompanyId", "Name");
            return View();
        }

        // POST: SalonSets/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SalonId,Name,Address,Phone,CompanyId")] SalonSet salonSet)
        {
            if (ModelState.IsValid)
            {
                db.SalonSet.Add(salonSet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CompanyId = new SelectList(db.CompanySet, "CompanyId", "Name", salonSet.CompanyId);
            return View(salonSet);
        }

        // GET: SalonSets/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalonSet salonSet = db.SalonSet.Find(id);
            if (salonSet == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyId = new SelectList(db.CompanySet, "CompanyId", "Name", salonSet.CompanyId);
            return View(salonSet);
        }

        // POST: SalonSets/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SalonId,Name,Address,Phone,CompanyId")] SalonSet salonSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(salonSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyId = new SelectList(db.CompanySet, "CompanyId", "Name", salonSet.CompanyId);
            return View(salonSet);
        }

        // GET: SalonSets/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalonSet salonSet = db.SalonSet.Find(id);
            if (salonSet == null)
            {
                return HttpNotFound();
            }
            return View(salonSet);
        }

        // POST: SalonSets/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SalonSet salonSet = db.SalonSet.Find(id);
            db.SalonSet.Remove(salonSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
