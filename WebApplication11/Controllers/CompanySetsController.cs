﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class CompanySetsController : Controller
    {
        private Model11 db = new Model11();

        // GET: CompanySets
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Index()
        {
            return View(db.CompanySet.ToList());
        }

        public ActionResult Upload()
        {
            var file = Request.Files["Filedata"];
            string savePath = Server.MapPath(@"~\UploadFiles\CompanySets\" + file.FileName);
            file.SaveAs(savePath);

            return Content(Url.Content(@"~\UploadFiles\CompanySets\" + file.FileName));
        }

        public JsonResult IsName(string Name)
        {
            return Json(!db.CompanySet.Any(x => x.Name == Name),
                                                 JsonRequestBehavior.AllowGet);
        }

        // GET: CompanySets/Details/5
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanySet companySet = db.CompanySet.Find(id);
            if (companySet == null)
            {
                return HttpNotFound();
            }
            return View(companySet);
        }


       


        // GET: CompanySets/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: CompanySets/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CompanyId,Name,address,inn,numnalog,kpp,rs,Bank,ks,bic,ogrn,numgos,okved,oktmo,okato,tel,mailaddress,gendir")] CompanySet companySet, HttpPostedFileBase file)
        {

            if (ModelState.IsValid)
            {
                db.CompanySet.Add(companySet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(companySet);
        }

        //    //Добавляем файл
        //{
        //    if (file == null)
        //    {
        //        // Не дает сохранить форму если в ней нет изменений
        //        // ModelState.AddModelError("CustomError", "Please select CV");
        //        return View();
        //    }

        //    if (!(file.ContentType == "image/jpeg" ||
        //        file.ContentType == "application/pdf"))
        //    {
        //        ModelState.AddModelError("CustomError", "Only .jpef and .pdf file allowed");
        //        return View();
        //    }
        //    if (ModelState.IsValid)
        //    {

        //        string fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
        //        file.SaveAs(Path.Combine(Server.MapPath("~/UploadedCV"), fileName));
        //        using (Model11 dc = new Model11())
        //        {
        //            db.Entry(companySet).State = EntityState.Modified;
        //            companySet.files = fileName;
        //            dc.CompanySet.Add(companySet);
        //            dc.SaveChanges();
        //            return RedirectToAction("Index");
        //        }
        //        // Очищает форму
        //        // ModelState.Clear();
        //        //companySet = null;
        //        //ViewBag.Message = "Successfully Done";

        //    }

        //    return View(companySet);
        //}

        // GET: CompanySets/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanySet companySet = db.CompanySet.Find(id);
            
            if (companySet == null)
            {
                return HttpNotFound();
            }
            return View(companySet);
        }

        // POST: CompanySets/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CompanyId,Name,address,inn,numnalog,kpp,rs,Bank,ks,bic,ogrn,numgos,okved,oktmo,okato,tel,mailaddress,gendir")] CompanySet companySet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(companySet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(companySet);
        }


        

        


        // GET: CompanySets/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanySet companySet = db.CompanySet.Find(id);
            if (companySet == null)
            {
                return HttpNotFound();
            }
            return View(companySet);
        }

        // POST: CompanySets/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CompanySet companySet = db.CompanySet.Find(id);
            db.CompanySet.Remove(companySet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
