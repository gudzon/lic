﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class ApkController : Controller
    {
        private Model11 db = new Model11();

        // GET: Apk
        public ActionResult Index()
        {
            return View(db.Apks.ToList());
        }

        // GET: Apk/Details/5
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Apk apk = db.Apks.Find(id);
            if (apk == null)
            {
                return HttpNotFound();
            }
            return View(apk);
        }

        // GET: Apk/Create
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Apk/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, ReadUser")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdApk,NameApk,DescriptionApk,VersionApk,FileApk")] Apk apk)
        {
            if (ModelState.IsValid)
            {
                db.Apks.Add(apk);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(apk);
        }

        // GET: Apk/Edit/5
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Apk apk = db.Apks.Find(id);
            if (apk == null)
            {
                return HttpNotFound();
            }
            return View(apk);
        }

        // POST: Apk/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Edit([Bind(Include = "IdApk,NameApk,DescriptionApk,VersionApk,FileApk")] Apk apk)
        {
            if (ModelState.IsValid)
            {
                db.Entry(apk).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(apk);
        }

        // GET: Apk/Delete/5
        [Authorize(Roles = "Admin, ReadUser")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Apk apk = db.Apks.Find(id);
            if (apk == null)
            {
                return HttpNotFound();
            }
            return View(apk);
        }

        // POST: Apk/Delete/5
        [Authorize(Roles = "Admin, ReadUser")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Apk apk = db.Apks.Find(id);
            db.Apks.Remove(apk);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
