﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication11.Models;

namespace WebApplication11.Controllers
{
    public class BuyLicSetsController : Controller
    {
        private Model11 db = new Model11();

        // GET: BuyLicSets
        public ActionResult Index()
        {
            var buyLicSet = db.BuyLicSet.Include(b => b.CompanySet).Include(b => b.ComputerSet).Include(b => b.ProviderSet).Include(b => b.SalonSet).Include(b => b.SoftCompanySet).Include(b => b.SoftSet);
            return View(buyLicSet.ToList());
        }

        // GET: BuyLicSets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BuyLicSet buyLicSet = db.BuyLicSet.Find(id);
            if (buyLicSet == null)
            {
                return HttpNotFound();
            }
            return View(buyLicSet);
        }

        // GET: BuyLicSets/Create
        public ActionResult Create()
        {
            ViewBag.CompanyId = new SelectList(db.CompanySet, "CompanyId", "Name");
            ViewBag.ComputerId = new SelectList(db.ComputerSet, "ComputerId", "Name");
            ViewBag.ProviderId = new SelectList(db.ProviderSet, "ProviderId", "Name");
            ViewBag.SalonId = new SelectList(db.SalonSet, "SalonId", "Name");
            ViewBag.SoftCompanyId = new SelectList(db.SoftCompanySet, "SoftCompanyId", "Name");
            ViewBag.SoftId = new SelectList(db.SoftSet, "SoftId", "Name");
            return View();
        }

        // POST: BuyLicSets/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BuyLicId,SoftId,Quantity,Price,SoftCompanyId,CompanyId,DateBuy,SalonId,ComputerId,DateActivation,Invoice,Docs,ProviderId")] BuyLicSet buyLicSet)
        {
            if (ModelState.IsValid)
            {
                db.BuyLicSet.Add(buyLicSet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CompanyId = new SelectList(db.CompanySet, "CompanyId", "Name", buyLicSet.CompanyId);
            ViewBag.ComputerId = new SelectList(db.ComputerSet, "ComputerId", "Name", buyLicSet.ComputerId);
            ViewBag.ProviderId = new SelectList(db.ProviderSet, "ProviderId", "Name", buyLicSet.ProviderId);
            ViewBag.SalonId = new SelectList(db.SalonSet, "SalonId", "Name", buyLicSet.SalonId);
            ViewBag.SoftCompanyId = new SelectList(db.SoftCompanySet, "SoftCompanyId", "Name", buyLicSet.SoftCompanyId);
            ViewBag.SoftId = new SelectList(db.SoftSet, "SoftId", "Name", buyLicSet.SoftId);
            return View(buyLicSet);
        }

        // GET: BuyLicSets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BuyLicSet buyLicSet = db.BuyLicSet.Find(id);
            if (buyLicSet == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyId = new SelectList(db.CompanySet, "CompanyId", "Name", buyLicSet.CompanyId);
            ViewBag.ComputerId = new SelectList(db.ComputerSet, "ComputerId", "Name", buyLicSet.ComputerId);
            ViewBag.ProviderId = new SelectList(db.ProviderSet, "ProviderId", "Name", buyLicSet.ProviderId);
            ViewBag.SalonId = new SelectList(db.SalonSet, "SalonId", "Name", buyLicSet.SalonId);
            ViewBag.SoftCompanyId = new SelectList(db.SoftCompanySet, "SoftCompanyId", "Name", buyLicSet.SoftCompanyId);
            ViewBag.SoftId = new SelectList(db.SoftSet, "SoftId", "Name", buyLicSet.SoftId);
            return View(buyLicSet);
        }

        // POST: BuyLicSets/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BuyLicId,SoftId,Quantity,Price,SoftCompanyId,CompanyId,DateBuy,SalonId,ComputerId,DateActivation,Invoice,Docs,ProviderId")] BuyLicSet buyLicSet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(buyLicSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyId = new SelectList(db.CompanySet, "CompanyId", "Name", buyLicSet.CompanyId);
            ViewBag.ComputerId = new SelectList(db.ComputerSet, "ComputerId", "Name", buyLicSet.ComputerId);
            ViewBag.ProviderId = new SelectList(db.ProviderSet, "ProviderId", "Name", buyLicSet.ProviderId);
            ViewBag.SalonId = new SelectList(db.SalonSet, "SalonId", "Name", buyLicSet.SalonId);
            ViewBag.SoftCompanyId = new SelectList(db.SoftCompanySet, "SoftCompanyId", "Name", buyLicSet.SoftCompanyId);
            ViewBag.SoftId = new SelectList(db.SoftSet, "SoftId", "Name", buyLicSet.SoftId);
            return View(buyLicSet);
        }

        // GET: BuyLicSets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BuyLicSet buyLicSet = db.BuyLicSet.Find(id);
            if (buyLicSet == null)
            {
                return HttpNotFound();
            }
            return View(buyLicSet);
        }

        // POST: BuyLicSets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BuyLicSet buyLicSet = db.BuyLicSet.Find(id);
            db.BuyLicSet.Remove(buyLicSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
